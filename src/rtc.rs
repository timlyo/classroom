use anyhow::anyhow;
use wasm_peers::many_to_many::NetworkManager;
use wasm_peers::{ConnectionType, SessionId, UserId};

const SIGNALING_SERVER_URL: &str = "ws://0.0.0.0:9001/many-to-many";
const STUN_SERVER_URL: &str = "stun:stun.aa.net.uk:3478";

pub fn get_rtc_client(
    on_message: impl FnMut(UserId, String) + Clone + 'static,
) -> anyhow::Result<NetworkManager> {
    let mut peer = NetworkManager::new(
        SIGNALING_SERVER_URL,
        SessionId::new("dummy-session-id".to_string()),
        ConnectionType::Stun {
            urls: STUN_SERVER_URL.to_string(),
        },
    )
    .map_err(|e| anyhow!("Failed to connect RTC client: {:?}", e))?;

    let peer_on_open = {
        move |user_id| {
            log::info!("connection to peer established: {:?}", user_id);
        }
    };

    peer.start(peer_on_open, on_message).unwrap();

    Ok(peer)
}
