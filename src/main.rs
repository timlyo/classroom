mod render;
mod rtc;

use dioxus::core::UiEvent;
use dioxus::events::MouseData;
use dioxus::prelude::*;
use futures_util::StreamExt;
use itertools::Itertools;
use lib0::any::Any;
use log::Level;
use uuid::Uuid;
use yrs::updates::decoder::Decode;
use yrs::updates::encoder::Encode;
use yrs::{Doc, PrelimArray, StateVector, Update};

fn main() {
    wasm_logger::init(wasm_logger::Config::new(Level::Info));
    log::info!("Starting");

    console_error_panic_hook::set_once();

    dioxus::web::launch(app);
}

fn app(cx: Scope) -> Element {
    let doc = use_ref(&cx, Doc::new);

    let current_line = use_state(&cx, || None);

    let peer_on_message = {
        let doc = doc.to_owned();
        move |_user_id, message| {
            if let Ok(message) = base64::decode(message) {
                let mut tn = doc.write().transact();
                let update = tn.encode_diff_v1(&StateVector::decode_v1(&message));
                tn.apply_update(Update::decode_v1(&update));
                log::info!("Updated docs");
            }
        }
    };

    let rtc = use_state(&cx, || {
        rtc::get_rtc_client(peer_on_message).expect("Failed RTC connection")
    });

    let update_peer_docs = use_coroutine(&cx, |mut rx| {
        let doc = doc.to_owned();
        let rtc = rtc.to_owned();

        async move {
            while let Some(()) = rx.next().await {
                let tn = doc.read().transact();
                let vec = tn.state_vector().encode_v1();
                rtc.send_message_to_all(&base64::encode(vec));
            }
        }
    });

    cx.render(rsx!(
        svg {
            onmousedown: move |event| svg_on_mouse_down(event, current_line, doc, update_peer_docs),
            onmousemove: move |event| svg_on_move(event, current_line, doc, update_peer_docs),
            onmouseup: move |event| svg_on_mouse_up(event, current_line, doc, update_peer_docs),
            id: "whiteboard",
            Lines{doc: doc.to_owned()}
        }
        DebugDoc{doc: doc.to_owned()}
    ))
}

#[allow(non_snake_case)]
#[inline_props]
fn DebugDoc(cx: Scope, doc: UseRef<Doc>) -> Element {
    let mut tn = doc.read().transact();
    let lines = tn.get_map("lines").to_json();

    cx.render(rsx! {
        pre {"lines: {lines:#?}"}
    })
}

fn value_to_vec(value: yrs::types::Value) -> Vec<(i64, i64)> {
    let array = value.to_yarray().unwrap();
    let line = array
        .iter()
        .map(|element| match element.to_json() {
            Any::Number(num) => num as i64,
            Any::BigInt(num) => num,
            _ => panic!("Wrong type"),
        })
        .chunks(2);

    line.into_iter()
        .filter_map(|chunk| chunk.into_iter().collect_tuple())
        .collect()
}

#[allow(non_snake_case)]
#[inline_props]
fn Lines(cx: Scope, doc: UseRef<Doc>) -> Element {
    let mut tn = doc.read().transact();

    let lines = tn.get_map("lines");
    let values = lines
        .iter()
        .map(|(id, line)| (id, value_to_vec(line)))
        .map(|(id, line)| {
            rsx! {
                Line {
                    line: line,
                    key: "{id}"
                }
            }
        });

    cx.render(rsx! {
        values
    })
}

#[allow(non_snake_case)]
#[inline_props]
fn Line(cx: Scope, line: Vec<(i64, i64)>) -> Element {
    let rendered = render::render_line(line);

    cx.render(rsx! {
        path{
            d: "{rendered}",
            stroke: "black",
        }
    })
}

fn svg_on_mouse_down(
    event: UiEvent<MouseData>,
    current_line: &UseState<Option<String>>,
    doc: &UseRef<Doc>,
    update_peer_docs: &CoroutineHandle<()>,
) {
    log::info!("Mouse down {} {}", event.client_x, event.client_y);
    let mut tn = doc.write().transact();

    let lines = tn.get_map("lines");
    let line_id = Uuid::new_v4().to_string();

    log::info!("New line: {line_id} {event:?}");

    let line = PrelimArray::from(vec![event.client_x, event.client_y]);
    lines.insert(&mut tn, line_id.as_str(), line);

    current_line.set(Some(line_id));
    update_peer_docs.send(());
}

fn svg_on_move(
    event: UiEvent<MouseData>,
    current_line: &UseState<Option<String>>,
    doc: &UseRef<Doc>,
    update_peer_docs: &CoroutineHandle<()>,
) {
    if let Some(current_line_id) = current_line.as_deref() {
        let mut tn = doc.write().transact();
        let lines = tn.get_map("lines");

        let current_line = lines.get(current_line_id).unwrap();

        let current_line = current_line.to_yarray().unwrap();
        current_line.push_back(&mut tn, event.client_x);
        current_line.push_back(&mut tn, event.client_y);
        update_peer_docs.send(());
    }
}

fn svg_on_mouse_up(
    event: UiEvent<MouseData>,
    current_line: &UseState<Option<String>>,
    _doc: &UseRef<Doc>,
    update_peer_docs: &CoroutineHandle<()>,
) {
    log::info!(
        "Mouse up {} {} {:?}",
        event.client_x,
        event.client_y,
        current_line.get()
    );
    current_line.set(None);
    update_peer_docs.send(());
}
