use itertools::Itertools;
use nalgebra::{Point2, Rotation2};

use std::ops::{Add, Mul};

const LINE_DEGREE: usize = 3;

#[derive(Copy, Clone, Debug)]
pub struct Point {
    pub x: f32,
    pub y: f32,
}

impl From<Point> for Point2<f32> {
    fn from(point: Point) -> Self {
        Self::from([point.x, point.y])
    }
}

impl Mul<f32> for Point {
    type Output = Point;
    fn mul(self, rhs: f32) -> Point {
        Point {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

impl Add for Point {
    type Output = Point;
    fn add(self, rhs: Point) -> Point {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

pub fn render_line(line: &Vec<(i64, i64)>) -> String {
    if line.len() <= LINE_DEGREE {
        return String::new();
    }

    let (start_x, start_y) = line.first().unwrap();

    let line = line
        .iter()
        .map(|(x, y)| Point {
            x: *x as f32,
            y: *y as f32,
        })
        .collect_vec();

    let line_length = line.len();

    let knots = (0..=(line_length + LINE_DEGREE))
        .map(|k| k as f32)
        .collect();
    let spline = bspline::BSpline::new(LINE_DEGREE, line, knots);

    let line = get_spline_range(spline.knot_domain(), 1.0)
        .map(|point| spline.point(point).into())
        .collect_vec();

    let outline = get_outline(line)
        .into_iter()
        .map(|(x, y)| format!("L {x} {y}"))
        .join(" ");

    format!("M {start_x} {start_y} {outline}")
}

fn get_outline(line: Vec<Point2<f32>>) -> Vec<(i64, i64)> {
    let left = line
        .iter()
        .tuple_windows()
        .filter(|(a, b)| a != b)
        .map(|(a, b)| (a, b - a))
        .map(|(pos, vector)| {
            let rotation = Rotation2::new(std::f32::consts::PI / 2.0);
            pos + rotation * vector.normalize() * 3.0
        });

    let right = line
        .iter()
        .tuple_windows()
        .filter(|(a, b)| a != b)
        .map(|(a, b)| (a, b - a))
        .map(|(pos, vector)| {
            let rotation = Rotation2::new(-std::f32::consts::PI / 2.0);
            pos + rotation * vector.normalize() * 3.0
        })
        .collect_vec();

    left.chain(right.into_iter().rev())
        .map(|vector| (vector.x.round() as i64, vector.y.round() as i64))
        .collect()
}

fn get_spline_range(domain: (f32, f32), step: f32) -> impl Iterator<Item = f32> {
    let scale = domain.1 - domain.0;
    let points_count = (scale / step) as usize;

    (0..points_count).map(move |point| (point as f32) * step + domain.0)
}

#[cfg(test)]
mod tests {
    use super::*;
    use bspline::BSpline;

    #[test]
    fn asdasda() {
        let degree = 2;

        let points = vec![
            Point { x: 0.0, y: 0.0 },
            Point { x: 1.0, y: 1.0 },
            Point { x: 3.0, y: 3.0 },
            Point { x: 4.0, y: 4.0 },
        ];

        let knots = (0..=(points.len() + degree)).map(|k| k as f32).collect();

        let spline = BSpline::new(degree, points, knots);

        dbg!(spline.knot_domain());

        for point in get_spline_range(spline.knot_domain(), 0.1) {
            dbg!((point, spline.point(point)));
        }
    }

    #[test]
    fn test_get_outline() {
        let line = vec![
            Point2::from([1.0, 1.0]),
            Point2::from([1.0, 3.0]),
            Point2::from([1.0, 5.0]),
        ];

        let outline = get_outline(line);

        assert_eq!(outline.len(), 4);

        assert_eq!(outline, vec![(0, 1), (0, 3), (2, 3), (2, 1)]);
    }
}
